#!/bin/bash

cd download/courses
for d in *
do
  g=$(egrep "^$d," ../../list-of-courses.txt | \
        uniq | \
        tr ' ' '-' | \
        tr -d ":\"'\(\)" | \
        sed -e 's%^[^,]*,%%' -e 's%&%and%' -e 's%-$%%' | \
        tr '[A-Z]' '[a-z]'
     )
  if [ "$d" == "$g" ]; then
    echo "# $d: no change"
  else
    echo mv $d $g
  fi
done | tee a | \
  egrep -v '^#' | tee a.sh
