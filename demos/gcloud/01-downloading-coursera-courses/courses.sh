#!/bin/bash

for course in \
  gcp-fundamentals \
  gcp-infrastructure-foundation \
  gcp-infrastructure-core-services \
  gcp-infrastructure-scaling-automation \
  preparing-cloud-associate-cloud-engineer-exam
do
  [ -d download/$course ] && continue
  echo $course
  coursera-dl --cauth $cauth \
    --subtitle-language en \
    --download-notebooks \
    --video-resolution 720p \
    --path {download/courses/,}$course
  echo "Sleeping..."
done
