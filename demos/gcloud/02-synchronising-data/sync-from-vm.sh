#!/bin/bash

cd `dirname $0`
source  ./env.sh
gcloud config configurations activate $project

rsync \
  --rsh ./ssh.sh \
  --recursive \
  --partial \
  --times \
  --perms \
  --links \
  --hard-links \
  --one-file-system \
  --progress $host:data . 