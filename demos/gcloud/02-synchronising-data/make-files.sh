#!/bin/bash

[ -d data ] && rm -rf data
for d in data/{0..3}/{a..c}/{5..6}
do
  mkdir -p $d
  string=""
  for i in {0..19}
  do
    segment=`printf %04x $RANDOM`
    string="${string}${segment}"
  done
  echo $string | tee $d/file.dat > /dev/null
done