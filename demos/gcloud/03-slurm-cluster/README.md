# Deploying a Slurm cluster on GCP

[Slurm](https://slurm.schedmd.com) is an open-source batch scheduler with a lively support community. It's used by many of the largest computers in the world, and is easy to learn. Many workflow managers (e.g. Nextflow) support Slurm out of the box, so if you're looking to run batch jobs in the cloud, this is a very easy way to get started.

The cluster consists of a **login** node, a **controller** node, and a configurable number of workers. The worker nodes are created on-demand, when there are jobs in the batch queue. They run the job, then, if there's no new job for them within 5 minutes, they're terminated. This means you can configure a rather large cluster and only pay for batch nodes when you're actually running jobs. There's some overhead to the startup and shutdown, but it's a good start.

This demo uses the sample code at https://github.com/SchedMD/slurm-gcp.git to deploy a Slurm cluster. Check that repository for full documentation, this README only covers the highlights.

There are two options for deploying Slurm from that repository, using Deployment Manager, and using Terraform. The Deployment Manager version is officially deprecated, however it still works, and that's what we use here.

## Configuring the project
There's wrapper code here to set up the project, based on the example in the **../00-managing-projects** directory.

First, edit **env.sh** to set the parent folder ID, based on where you're allowed to create projects in the EBI GCP organisation.

Then there's some logic to set the project name, either using a recorded project name in the **.projectname** file or calculating one based on the username and date. If the name is calculated, it's stored in the **.projectname** file for future use. If you want to use a specific project name, just echo it into the **.projectname** file before you start.

Finally, you need to set the **cluster_name**. This is used in a few places, in particular there should be a **${cluster_name}.yaml** file, which we cover later. The example here uses a cluster called **ips2**, hence the **ips2.yaml** file.

Once that's done, run the **./create-project.sh** script to create the project and enable a few APIs etc. You'll need to have the **gcloud SDK** installed for this, see the [README in the managing-projects directory](../00-managing-projects/README.md) for more details.

## Configure the cluster
The **${cluster_name}.yaml** file is a Deployment Manager configuration file which defines the parameters of your Slurm cluster. In this example, we create a cluster called **ips2**, hence the **ips2.yaml** file.

Full documentation for the file contents is with the **slurm-gcp** repository, but this is a fully working example that you can use out of the box. That said, there are a few things you'll probably want to modify:
  - For the controller and login machines, you can specify the machine type, the disk type and size, and a few other parameters.
  - the batch workers are based on a disk image which is created by the deployment manager. You can tune the parameters of the image if you want.

Slurm uses __partitions__ where LSF uses __queues__. Each partition is associated with different machine types (number of CPUs, amount of memory...) and the number of machines associated with each partition can be configured separately. The **ips2.yaml** example configures 4 partitions. The **debug** partition is the default (first in the list), then there are 3 partitions which differ only in the amount of memory they have. They all have 16 CPUs, but they have 16, 32, or 64 GB RAM. Note that the **mem32** partition actually uses a custom machine type, you won't find that machine type listed in Google's documentation!

## Creating the cluster
Once you have the cluster configured, run the **create-cluster.sh** script. This does some sanity checks and downloads the slurm-gcp repository if it's not there. It copies the config file into place, substituting a couple of values from the **env.sh** file. Then it uses Deployment Manager to create the cluster, and finally copies a slurm test script onto the login node.

Deployment Manager will terminate successfully after the cluster is deployed, however it can take up to 10 minutes before Slurm is available, since there's a lot more initialising going on behind the scenes. Log in to the login node by running the **./ssh.sh** script and run **ls** to see if the **test-slurm.sh** script is there. If not, log out, wait a few minutes, and try again. Once slurm is fully installed the disks from the controller node are exported and mounted on the login node and you will be able to see the test script.

## Running jobs
There's a very simple test script, **test-slurm.sh**, which will detect the list of partitions and submit several copies of a small job to each partition. You can use this to check that things are working. If all goes well, track the state of your jobs with the **squeue** command, and find their output in the **$HOME/slurm/test** directory on the login node when they're done.

Your /home directory is shared with the batch nodes, so you can install your software and data there and expect to find it when you run.

Spinning up batch nodes can take some time, a few minutes, since it's not only GCP that has to work but slurm too that has to notice the new nodes are online. Be patient, it'll get there!

Slurm will, by default in this configuration, submit each job to a separate node. This means if you allow 10 batch nodes in a partition and you submit 10 jobs, you will spin up 10 worker nodes. If your jobs are extremely short,like the test example here, that's a huge overhead, but if your jobs run for longer than 20 minutes or so it's relatively small.

## Recommendations & best practices
If you want to change some parameters of the cluster, such as the size of the controller or login nodes, you can do that by editing the yaml config file and issuing a **gcloud deployments-manager deployment update ...** command. If you want to change the number of partitions or the machine types associated with the partition, that won't work, because that information is used post-deployment, and the deployment manager won't detect the need to rebuild the cluster.

If you do decide to update your cluster, be aware that this may destroy/recreate, rather than update in place. You may lose any software or data you've copied onto the cluster.

Consider your slurm clusters disposable. You can create new ones any time by creating a new yaml config file and running the deployment manager, you can get the commands from the scripts here. As long as your software is easy to install there's little overhead to this, so you can create and destroy clusters whenever you like.

To destroy your cluster, run **gcloud deployments-manager delete ${cluster_name} --quiet**. Note that this will fail if there are any batch worker nodes still running, since the deployment manager didn't create them, and won't destroy them, but can't then destroy the network components that those workers are still using.

Start with a small slurm cluster, with only a few batch nodes, and experiment until you have the right types for your workflow. Then destroy your test cluster and build a bigger one for production running.

Start with a small disk on the controller node, and avoid the temptation to make the controller and login machines too powerful. These nodes, and the disk they have, are permanently running, so cost money even if you don't use the cluster. Again, you can destroy your cluster and create a new one easily enough, so there's no need to worry about getting the size just right straight away.

When you move to production running, think carefully about your cluster size and your needs for job completion, only allow as many workers as you need to get your work done in time. E.g., running 1000 jobs on 1000 machines will cost a lot more than running them on 10 machines and letting them queue, because the longer-running machines will benefit from sustained use discounts, which can cut the cost of computing by 30%.

Running on fewer machines with lots of jobs in the queue will also amortise the startup/shutdown time of the batch nodes, because as soon as one job ends another can start. If your jobs are short, this can have a big effect.

Another way you can reduce costs is to shut down your cluster when you're not using it. Use **gcloud compute instances stop ${cluster_name}-{login0,controller}** to stop them, and change 'stop' to 'start' when you want to restart them.

If you really want bonus points, you could code up a custom metric to record the number of slurm jobs every minute, add an alert if the number of jobs is zero for more than an hour, then add a cloud function to trigger on that alert and shut down the cluster automatically. That would be a cool exercise!