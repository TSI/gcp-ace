#!/bin/bash

cd `dirname $0`

#
# Source the environment script to pick up the variable definitions.
. ./env.sh

#
# Check if this project already exists or not. If it does, don't try
# to create it again.
#
# N.B. the filter argument can take wildcards, otherwise it's an exact
# and complete match, not a substring match
#
# See 'gcloud topic filters' for help
exists=$(gcloud projects list --filter="name=$project")
if [ "$exists" != "" ]; then
  echo "Project '$project' exists..."
  exit 0
fi

echo "Project '$project' does not exist: creating..."
gcloud projects create $project \
  --folder=$folder \
  --labels=owner=$USER # labels are arbitrary key/value pairs

#
# Link the billing account to the project, or we are limited to free resources only
gcloud beta billing projects link $project --billing-account=$billing_account

#
# Now that the project exists, create a configuration locally so I can switch to
# it easily. I prefer to overspecify, rather than risk any defaults creeping in
gcloud config configurations create $project
gcloud config set core/project $project
gcloud config set core/account ${USER}@ebi.ac.uk
gcloud config set compute/region $region
gcloud config set compute/zone $zone

#
# It's not enough to create a project, you have to explicitly enable the services
# you want to use. You do this by enabling APIs.
#
# 'gcloud services list --available' gives a full list of the 300+ services that exist!
gcloud services enable \
  deploymentmanager.googleapis.com \
  compute.googleapis.com \
  file.googleapis.com