export CLOUDSDK_PYTHON=$(which python3)
cd `dirname $0`
source ./env.sh
gcloud config configurations activate $project
shift
exec gcloud compute ssh "${cluster_name}-login0" -- "$@"
