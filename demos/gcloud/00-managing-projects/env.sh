#
# These are 'mandatory' parameters, in that you should really set them for every project you create
#

#
# The billing account is the only one we're allowed to use with our EBI accounts.
billing_account="00F8FC-525826-16C3A7" # Main EBI Billing Account

#
# The folder is the location in the GCP  EBI organisation tree. Get it from the
# GCP console -> IAM & Admin -> Manage Resources view
folder=187759886721 # ebi.ac.uk -> Technical Services -> TSI -> Cloud Certification

#
# Choose a name for your project. Here I just build a generic name that is likely
# to be unique by adding the current date to it.
project_name_file=".projectname"
if [ -f $project_name_file ]; then
  project=`cat $project_name_file`
else
  timestamp=`date +%g-%m-%d`
  # timestamp=`date +%g-%m-%d-%H:%M` # If I want higher timestamp resolution...
  project="gcp-ace-$USER-$timestamp"
  echo $project > $project_name_file
fi

#
# Set a region and zone. These are arbitrary, so I choose London
region="europe-west2"
zone="${region}-c"

#
# The parameters below are optional, depending on what you intend to do with your project
#

#
# I'm going to create a VM, so here are some parameters for it, just for fun.
# Note that VM names must all be in lowercase!
host="`echo ${USER}-vm | tr [A-Z] [a-z]`"
instance="n2-standard-2"
disk_size="100GB"
